import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { ITask } from '@/infra/types/interfaces/task.interface'
import { createTask, getTasks, updateTask } from '@/domain/tasks/api/tasksRepository'

export const useTaskStore = defineStore('task', {
  state: () => ({
    tasks: [] as ITask[]
  }),
  getters: {
    finishedTasks(state) {
      return state.tasks.filter((task: ITask) => task.isCompleted)
    },
    unfinishedTasks(state) {
      return state.tasks.filter((task: ITask) => !task.isCompleted)
    }
  },
  actions: {
    addTask(task: ITask) {
      // save to storage
      createTask(task).then((result) => {
        // mutate state
        this.tasks.push({ ...result })
      })
    },
    editTask(task: ITask) {
      updateTask(task).then((result) => {
        this.tasks = this.tasks.map((oldTask: ITask) => {
          if (oldTask.id === task.id) return result
          return oldTask
        })
      })
    },
    allTasks() {
      getTasks().then((result) => {
        this.tasks = result
        console.log(result)
      })
    }
  }
})
