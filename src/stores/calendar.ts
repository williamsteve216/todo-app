import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useCalendarStore = defineStore('calendar', {
  state: () => ({
    date: new Date() as Date | null
  }),
  getters: {
    getActiveDate(state) {
      return state.date
    }
  },
  actions: {
    changeDate(date: Date) {
      this.date = date
    }
  }
})
