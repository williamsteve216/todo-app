export interface ITask {
  id?: number
  name?: string
  isCompleted?: boolean
  date?: string
  priority?: number
}
