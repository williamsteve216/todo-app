export interface IDate {
  day: IDay
  month: IMonth
  year: IYear
}

export interface IDay {
  value: number // valeur du jour(date)
  title: string
  numero: number // Nummero  du jour de la semaine
  monthNumber: number
  year: number
  isPicked: boolean
}

export interface IWeek {
  days: IDay[]
}

export interface IMonth {
  value: number
  days: IDay[]
  endDay: number
  title: string
}

export interface IYear {
  value: number
  months: IMonth[]
}
