import type { IDay, IMonth, IWeek } from '../types/interfaces/date.interace'

export const isALeapYear = (year: number): boolean => {
  if (year % 400 != 0) return false
  if (year % 100 == 0 && year % 400 == 0) return true
  if (year % 100 == 0) return false
  if (year % 4 == 0) return true
  return false
}

export const getDay = (date: Date): IDay => {
  const day: IDay = {
    value: 0,
    title: '',
    numero: 0,
    monthNumber: 0,
    year: 0,
    isPicked: false
  }
  const days: string[] = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']
  day.title = days[date.getDay()]
  day.numero = date.getDay()
  day.value = date.getDate()
  day.monthNumber = date.getMonth()
  day.year = date.getFullYear()
  return day
}

export const generateWeek = (year: number, month: IMonth, day: IDay): IWeek => {
  const week: IWeek = {
    days: []
  }
  const numOfDay = day.numero
  let compt = 1
  const dayBefore: IDay[] = []
  const dayAfter: IDay[] = []
  for (let i = numOfDay; i > 0; i--) {
    const newValueOfDay = day.value - compt
    if (newValueOfDay <= 0) {
      //Gestion du cas de janvier
      const newMonth: IMonth | null =
        month.value == 0 ? generateMonth(year - 1, 11) : generateMonth(year, month.value - 1)
      if (newMonth) {
        const newDay: IDay = newMonth.days[newMonth.endDay - compt]
        dayBefore.push(newDay)
      }
    } else {
      const newDay: IDay = month.days[newValueOfDay - 1]
      dayBefore.push(newDay)
    }
    compt++
  }
  compt = 1
  let j = 1
  for (let i = numOfDay; i < 6; i++) {
    const newValueOfDay = day.value + compt //31+1
    if (newValueOfDay > month.endDay) {
      const newMonth: IMonth | null =
        month.value == 11 ? generateMonth(year + 1, 0) : generateMonth(year, month.value + 1)
      if (newMonth) {
        // console.log(month.value + 1)
        const newDay: IDay = newMonth.days[j - 1]
        dayAfter.push(newDay)
      }
      j++
    } else {
      const newDay: IDay = month.days[newValueOfDay - 1]
      dayAfter.push(newDay)
    }
    compt++
  }
  week.days = [...dayBefore.reverse(), day, ...dayAfter]
  return week
}

export const getLastDayOfMonth = (year: number, numberOfMonth: number): number => {
  // 0<=numberOfMonth<=11
  const day = new Date(year, numberOfMonth + 1, 0)
  return day.getDate()
}

export const generateMonth = (year: number, numberOfMonth: number): IMonth | null => {
  if (numberOfMonth >= 0 && numberOfMonth < 12) {
    // 0<=numberOfMonth<=11

    const lastDayOfMonth = getLastDayOfMonth(year, numberOfMonth)
    const months: string[] = [
      'Janvier',
      'Fevrier',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Aout',
      'Septembre',
      'Octobre',
      'Novembre',
      'Decembre'
    ]
    const month: IMonth = {
      value: numberOfMonth,
      days: [],
      endDay: lastDayOfMonth,
      title: months[numberOfMonth]
    }
    for (let i = 1; i <= lastDayOfMonth; i++) {
      month.days.push(getDay(new Date(year, numberOfMonth, i)))
    }
    return month
  }
  return null
}

export const isDayEqual = (day1: IDay, day2: IDay) => {
  return (
    day1.value === day2.value && day1.monthNumber === day2.monthNumber && day1.year === day2.year
  )
}
