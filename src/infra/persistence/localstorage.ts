export const LocalStorage = {
  save: (key: string, data: any) => {
    localStorage.setItem(key, JSON.stringify(data))
  },
  retrieve: (key: string): any => {
    return localStorage.getItem(key)
  },
  remove: (key: string) => {
    localStorage.removeItem(key)
  },
  clear: () => {
    localStorage.clear()
  }
}
