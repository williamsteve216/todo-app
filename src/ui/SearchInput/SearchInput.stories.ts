import type { Meta, StoryObj } from '@storybook/vue3'

import SearchInput from './SearchInput.vue'

const meta = {
  title: 'UI/SearchInput',
  tags: ['autodocs'],
  component: SearchInput,
  argTypes: {
    placeholder: { name: 'placeholder', required: true },
    background: { control: 'color', required: false },
    color: { name: 'color' }
  },
  args: {
    placeholder: 'Search anything',
    background: '#242424',
    color: '#ffffff'
  }
} satisfies Meta<typeof SearchInput>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    placeholder: 'Search anything',
    background: '#242424',
    color: '#ffffff'
  }
}
