import type { Meta, StoryObj } from '@storybook/vue3'

import Badge from './Badge.vue'

const meta = {
  title: 'UI/Badge',
  tags: ['autodocs'],
  component: Badge,
  argTypes: {
    title: { name: 'title', control: 'string' }
  },
  args: {
    title: 'Priority 01'
  }
} satisfies Meta<typeof Badge>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    title: 'Default'
  }
}
