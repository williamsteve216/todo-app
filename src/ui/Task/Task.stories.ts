import type { Meta, StoryObj } from '@storybook/vue3'

import Task from './Task.vue'

const meta = {
  title: 'UI/Task',
  tags: ['autodocs'],
  component: Task,
  argTypes: {
    isCompleted: { conrol: 'boolean' },
    task: { required: true }
  },
  args: {
    isCompleted: false,
    task: {
      isCompleted: false,
      name: 'Example of task',
      date: new Date()
    }
  }
} satisfies Meta<typeof Task>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    task: {
      isCompleted: false,
      name: 'Example of task',
      date: new Date()
    }
  }
}

export const Compled: Story = {
  args: {
    task: {
      isCompleted: true,
      name: 'Example of task',
      date: new Date()
    }
  }
}
