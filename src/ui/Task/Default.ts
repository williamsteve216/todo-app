import { Story } from './Task.stories'

export const Default: Story = {
  args: {
    isCompleted: false
  }
}
