import type { Meta, StoryObj } from '@storybook/vue3'

import DayPicker from './DayPicker.vue'

const meta = {
  title: 'UI/DayPicker',
  tags: ['autodocs'],
  component: DayPicker,
  argTypes: {
    color: { control: 'color', required: false },
    isToday: { control: 'boolean', required: false },
    isPicked: { control: 'boolean', required: false },
    isPresentOnMonth: { control: 'boolean', required: false },
    backgroundPicker: { control: 'color', required: false },
    backgroundOrtherMonth: { control: 'color', required: false },
    colorOfToday: { control: 'color', required: false }
  },
  args: {
    color: '#000000',
    isToday: false,
    isPicked: false,
    isPresentOnMonth: true,
    backgroundPicker: 'transparent',
    backgroundOrtherMonth: '#efefef',
    colorOfToday: '#0094ff'
  }
} satisfies Meta<typeof DayPicker>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    color: '#000000',
    isToday: false,
    isPicked: false,
    isPresentOnMonth: true,
    backgroundPicker: 'transparent',
    backgroundOrtherMonth: '#efefef'
  }
}

export const PickerDay: Story = {
  args: {
    isPicked: true,
    backgroundPicker: '#000000',
    color: '#ffffff'
  }
}

export const Today: Story = {
  args: {
    isToday: true,
    colorOfToday: '#0094ff'
  }
}

export const OrtherMonth: Story = {
  args: {
    isPresentOnMonth: false,
    backgroundOrtherMonth: '#efefef'
  }
}
