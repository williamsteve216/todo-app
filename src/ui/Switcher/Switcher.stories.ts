import type { Meta, StoryObj } from '@storybook/vue3'

import Switcher from './Switcher.vue'

const meta = {
  title: 'UI/Switcher',
  tags: ['autodocs'],
  component: Switcher,
  argTypes: {},
  args: {}
} satisfies Meta<typeof Switcher>

export default meta

type Story = StoryObj<typeof meta>
export const Default: Story = {
  args: {}
}
