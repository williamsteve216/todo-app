import type { Meta, StoryObj } from '@storybook/vue3'

import RadioButton from './RadioButton.vue'

const meta = {
  title: 'UI/RadioButton',
  tags: ['autodocs'],
  component: RadioButton,
  argTypes: {
    isCheck: { control: 'boolean', required: false }
  },
  args: {
    isCheck: false
  }
} satisfies Meta<typeof RadioButton>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    isCheck: false
  }
}

export const Checked: Story = {
  args: {
    isCheck: true
  }
}
