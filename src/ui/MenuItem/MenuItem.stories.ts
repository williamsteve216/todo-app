import type { Meta, StoryObj } from '@storybook/vue3'
import MenuItem from './MenuItem.vue'

const meta = {
  title: 'UI/MenuItem',
  tags: ['autodocs'],
  component: MenuItem,
  argTypes: {
    isActivate: { name: 'isActivate', required: false, control: 'boolean' },
    color: { control: 'color', required: false },
    activatedColor: { control: 'color', required: false }
  },
  args: {
    isActivate: false,
    color: '#000000',
    activatedColor: '#000000'
  }
} satisfies Meta<typeof MenuItem>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    isActivate: false,
    color: '#000000'
  }
}

export const ActivateColor: Story = {
  args: {
    isActivate: true
  }
}
