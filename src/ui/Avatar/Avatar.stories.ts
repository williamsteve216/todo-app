import type { Meta, StoryObj } from '@storybook/vue3'
import Avatar from './Avatar.vue'

const meta = {
  title: 'UI/Avatar',
  tags: ['autodocs'],
  component: Avatar,
  argTypes: {
    image: { name: 'Image', required: true },
    type: {
      control: 'select',
      options: ['square', 'rounded', 'rounded-full']
    }
  },
  args: {
    image: 'https://i.pravatar.cc/150?img=3',
    type: 'square'
  }
} satisfies Meta<typeof Avatar>
export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    type: 'square',
    image: 'https://i.pravatar.cc/150?img=3'
  }
}
