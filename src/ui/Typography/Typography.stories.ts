import type { Meta, StoryObj } from '@storybook/vue3'

import Typography from './Typography.vue'

const meta = {
  title: 'UI/Typography',
  tags: ['autodocs'],
  component: Typography,
  argTypes: {
    size: {
      control: 'select',
      options: ['v-small', 'small', 'medium', 'large', 'v-large', 'ex-large']
    }
  },
  args: {
    size: 'small'
  }
} satisfies Meta<typeof Typography>

export default meta

type Story = StoryObj<typeof meta>

export const VerySmall: Story = {
  args: {
    size: 'v-small'
  }
}
export const Large: Story = {
  args: {
    size: 'large'
  }
}
