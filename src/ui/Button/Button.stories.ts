import type { Meta, StoryObj } from '@storybook/vue3'

import Button from './Button.vue'

const meta = {
  title: 'UI/Button',
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/vue/writing-docs/autodocs
  tags: ['autodocs'],
  component: Button,
  argTypes: {
    outlined: { control: 'boolean' },
    hasBorder: { control: 'boolean' },
    color: { control: 'color' },
    bgColor: { control: 'color' }
  },
  args: { outlined: false, color: '#ffffff', bgColor: '#0866f3' } // default value
} satisfies Meta<typeof Button>

export default meta
type Story = StoryObj<typeof meta>

export const Primary: Story = {
  args: {
    outlined: false,
    hasBorder: false
  }
}

export const Outlined: Story = {
  args: {
    outlined: true,
    hasBorder: true,
    color: '#000000'
  }
}

export const EmptyButtun: Story = {
  args: {
    outlined: true,
    hasBorder: false,
    color: '#000000'
  }
}
