import type { Meta, StoryObj } from '@storybook/vue3'

import IconButton from './IconButton.vue'

const meta = {
  title: 'UI/IconButton',
  tags: ['autodocs'],
  component: IconButton,
  argTypes: {
    outlined: { control: 'boolean' },
    color: { control: 'color' },
    backgroundColor: { control: 'color' }
  },
  args: {
    outlined: false,
    color: '#000'
  }
} satisfies Meta<typeof IconButton>

export default meta
type Story = StoryObj<typeof meta>

export const Outlined: Story = {
  args: {
    outlined: true,
    color: '#000'
  }
}

export const Default: Story = {
  args: {
    outlined: false,
    color: '#000'
  }
}

export const WithBackground: Story = {
  args: {
    outlined: false,
    color: '#fff',
    backgroundColor: '#0866f3'
  }
}
