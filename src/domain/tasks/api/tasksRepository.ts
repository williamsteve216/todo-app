import { LocalStorage } from '@/infra/persistence/localstorage'
import type { ITask } from '@/infra/types/interfaces/task.interface'

// create task
export const createTask = (task: ITask): Promise<ITask> => {
  return new Promise(async (resolve, reject) => {
    try {
      const newTask: ITask = {
        ...task,
        id: new Date().getTime()
      }
      const allTasks: ITask[] = await getTasks()
      allTasks.push(newTask)
      LocalStorage.save('tasks', allTasks)
      resolve(newTask)
    } catch (error) {}
  })
}

// update task
export const updateTask = (task: ITask): Promise<ITask> => {
  return new Promise(async (resolve, reject) => {
    console.log(task)
    if (!task.id) reject('Id not found')
    try {
      const foundTask: ITask = await getTask(task.id)
      const newTask: ITask = {
        ...foundTask,
        ...task,
        id: foundTask.id
      }
      const allTasks: ITask[] = await getTasks()
      const result = allTasks.map((oldTask) => {
        if (oldTask.id === task.id) return newTask
        return oldTask
      })
      LocalStorage.save('tasks', result)
      resolve(await getTask(task.id))
    } catch (error) {
      console.log(error)
      reject(error)
    }
  })
}

// get task
export const getTask = (taskId: number | undefined): Promise<ITask> => {
  return new Promise(async (resolve, reject) => {
    if (!taskId) reject('Id not found')
    try {
      const allTasks: ITask[] = await getTasks()
      const foundTask: ITask | undefined = allTasks.find((task) => task.id === taskId)
      if (!foundTask) reject('not found')
      else resolve(foundTask)
    } catch (error) {
      reject(error)
    }
  })
}

// get all task
export const getTasks = (): Promise<ITask[]> => {
  return new Promise((resolve, reject) => {
    const allTasks: ITask[] = LocalStorage.retrieve('tasks')
      ? JSON.parse(LocalStorage.retrieve('tasks'))
      : []
    resolve(allTasks)
  })
}

// delete task
export const deleteTask = (taskId: string) => {}
